import React from 'react';
import './App.css';
import { Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Calculator from './components/Calculator';
import ResultHistory from './components/ResultHistory';


function App() {
  return (
    <React.Fragment>
      <h5 style={{textAlign : "center"}}>Calculator</h5>
        <Row className='mainRow'>
          <Col>
            <Calculator />
          </Col>
          <Col>
            <ResultHistory />
          </Col>
        </Row>
    </React.Fragment>
  );
}

export default App;
